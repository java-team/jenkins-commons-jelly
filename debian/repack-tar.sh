#/bin/sh -e

VERSION=$2
TAR=../jenkins-commons-jelly_$VERSION.orig.tar.xz
DIR=jenkins-commons-jelly-$VERSION
mkdir -p $DIR

# Unpack ready fo re-packing
tar -xzf $3 -C $DIR --strip-components=1
rm $3

# Move core of jelly into subdirectory
# This is required to allow all jelly
# components to be built from a single code
# base
mkdir -p ${DIR}/jelly-core
for file in $(cat debian/jelly-core.files)
do
	mv ${DIR}/${file} ${DIR}/jelly-core
done

# Repack excluding stuff we don't need
XZ_OPT=--best tar -c -J -v -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude 'CVS' \
    --exclude '.svn' \
    $DIR
rm -rf $DIR
